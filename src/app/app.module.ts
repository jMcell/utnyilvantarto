import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PartnersComponent } from './partners/partners.component';
import { DriveRoutesComponent } from './drive-routes/drive-routes.component';
import {RouterModule} from '@angular/router';
import {PartnersEditComponent} from './partners/partners-edit/partners-edit.component';
import { CarListComponent } from './partners/car-list/car-list.component';
import {AppRoutingModule} from './app-routing.module';
import {CarListEditComponent} from './partners/car-list/car-list-edit/car-list-edit.component';
import {DriveRoutesListComponent} from './partners/car-list/drive-routes-list/drive-routes-list.component';
import {DriveRoutesEditComponent} from './partners/car-list/drive-routes-list/drive-routes-edit/drive-routes-edit.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PartnersComponent,
    CarListComponent,
    CarListEditComponent,
    DriveRoutesListComponent,
    DriveRoutesEditComponent,
    DriveRoutesComponent,
    PartnersEditComponent,
    CarListComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule,
        AppRoutingModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
