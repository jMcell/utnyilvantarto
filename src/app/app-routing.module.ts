import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PartnersComponent} from './partners/partners.component';
import {DriveRoutesComponent} from './drive-routes/drive-routes.component';
import {CarListComponent} from './partners/car-list/car-list.component';
import {DriveRoutesListComponent} from './partners/car-list/drive-routes-list/drive-routes-list.component';
import {PartnersEditComponent} from './partners/partners-edit/partners-edit.component';
import {DriveRoutesEditComponent} from './partners/car-list/drive-routes-list/drive-routes-edit/drive-routes-edit.component';
import {CarListEditComponent} from './partners/car-list/car-list-edit/car-list-edit.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/partners', pathMatch: 'full' },
  { path: 'partners', component: PartnersComponent, children: [
      {path: 'new', component: PartnersEditComponent},
      {path: 'edit/:id', component: PartnersEditComponent},
      {path: ':partnerId', component: CarListComponent, children: [
          {path: 'new', component: CarListEditComponent},
          {path: 'edit/:id', component: CarListEditComponent},
          {path: ':license', component: DriveRoutesListComponent, children: [
              {path: 'new', component: DriveRoutesEditComponent},
              {path: 'edit/:id', component: DriveRoutesEditComponent}
            ]}
        ]},
    ] },
  { path: 'drive-routes', component: DriveRoutesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
