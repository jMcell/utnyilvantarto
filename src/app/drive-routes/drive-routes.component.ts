import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-drive-routes',
  templateUrl: './drive-routes.component.html',
  styleUrls: ['./drive-routes.component.css']
})
export class DriveRoutesComponent implements OnInit {
  driveRoutes;
  vehicle;
  fuelSum;
  distanceSum;
  fuelConsumption;
  selectedVehicle;
  tmpDriveRoutes: {time: string, vehicle: string, partner: string, length: number }[] = [];
  constructor() { }

  ngOnInit(): void {
    this.driveRoutes = JSON.parse(localStorage.getItem('driveRoutes'));
    this.vehicle = JSON.parse(localStorage.getItem('vehicle'));
  }

  calculateData(licensePlate: string) {
    this.fuelSum = 0;
    this.distanceSum = 0;
    this.fuelConsumption = 0;
    this.selectedVehicle = licensePlate;
    for (const vehicle of this.vehicle){
      if (vehicle.licensePlate === licensePlate){
        this.fuelConsumption = vehicle.fuelConsumption;
        break;
      }
    }
    for (const driveroute of this.driveRoutes){
      if (driveroute.vehicle === licensePlate){
        this.distanceSum += driveroute.length;
      }
    }
    this.fuelSum = (this.distanceSum / 100) * this.fuelConsumption;
    console.log(this.fuelSum + ' ' + this.distanceSum + ' ' + this.fuelConsumption);
  }

  filterVehicle(h: NgForm) {
    this.tmpDriveRoutes = [];
    for (const driveroutes of JSON.parse(localStorage.getItem('driveRoutes'))){
      if (driveroutes.vehicle === h.value.licensePlate){
        this.tmpDriveRoutes.push(driveroutes);
      }
    }
    this.driveRoutes = this.tmpDriveRoutes;
    console.log(this.driveRoutes);
  }

  filterDate(f: NgForm) {
    this.tmpDriveRoutes = [];
    for (const driveroutes of JSON.parse(localStorage.getItem('driveRoutes'))){
      if (driveroutes.time >= f.value.startDate && driveroutes.time <= f.value.endDate){
        this.tmpDriveRoutes.push(driveroutes);
      }
    }
    this.driveRoutes = this.tmpDriveRoutes;
    console.log(this.driveRoutes);
  }
}
