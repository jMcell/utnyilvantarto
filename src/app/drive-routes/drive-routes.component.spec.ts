import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriveRoutesComponent } from './drive-routes.component';

describe('DriveRoutesComponent', () => {
  let component: DriveRoutesComponent;
  let fixture: ComponentFixture<DriveRoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriveRoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriveRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
