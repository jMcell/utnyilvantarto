import {Component, Input, OnInit, Output} from '@angular/core';
import {PartnersService} from './partners.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css'],
  providers: [PartnersService]
})
export class PartnersComponent implements OnInit {
  visible: boolean;
  partner: {name: string, address: string, id: number}[] = [];
  constructor(private partnersService: PartnersService,
              private router: Router) { }

  ngOnInit(): void {
    this.visible = this.partnersService.visibe;
    this.partner = JSON.parse(localStorage.getItem('partner'));
  }
  switchVisible(){
    this.visible = !this.visible;
  }
}
