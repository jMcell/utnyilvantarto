import { Component, OnInit } from '@angular/core';
import {PartnersService} from '../../partners.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-drive-routes',
  templateUrl: './drive-routes-list.component.html',
  styleUrls: ['./drive-routes-list.component.css'],
  providers: [PartnersService]
})
export class DriveRoutesListComponent implements OnInit {
  visible = true;
  driveRoutes: {time: string, vehicle: string, partner: string, length: number }[] = [];
  licensePlate: string;
  constructor(private partnersService: PartnersService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.licensePlate = params.license;
        }
      );
    this.driveRoutes = JSON.parse(localStorage.getItem('driveRoutes'));
  }

  switchVisible(){
    this.visible = !this.visible;
  }
}
