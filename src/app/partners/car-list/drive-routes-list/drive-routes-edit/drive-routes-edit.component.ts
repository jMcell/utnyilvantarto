import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {DriveRoutesListComponent} from '../drive-routes-list.component';
import {NgForm} from '@angular/forms';
import {PartnersService} from '../../../partners.service';
import {PartnersComponent} from '../../../partners.component';
import {CarListComponent} from '../../car-list.component';

@Component({
  selector: 'app-edit',
  templateUrl: './drive-routes-edit.component.html',
  styleUrls: ['./drive-routes-edit.component.css']
})
export class DriveRoutesEditComponent implements OnInit, AfterViewInit {
  editMode: boolean;
  @ViewChild('f', {static: false}) driveRoutesForm: NgForm;
  path: string;
  id: number;
  driveRoutes: {
    time: string,
    vehicle: string,
    partner: string,
    length: number
  };
  constructor(private route: ActivatedRoute,
              private driveRoutesListComponent: DriveRoutesListComponent,
              private router: Router,
              private partnersService: PartnersService) { }

  ngOnInit(): void {
    if (this.driveRoutesListComponent.visible === true){
      this.driveRoutesListComponent.switchVisible();
    }
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params.id;
      }
    );
    if (this.id == null){
      this.path = '../';
    }else {
      this.path = '../../';
    }
  }
  ngAfterViewInit(): void{
    if (this.id == null){
      this.editMode = false;
    }else {
      this.editMode = true;
      this.driveRoutes = this.partnersService.getDriveRoutes('' + this.id);
      setTimeout(() => {
        this.driveRoutesForm.setValue({
          distance: this.driveRoutes.length,
          time: this.driveRoutes.time
        });
      });
    }
  }
  finish(form: NgForm){
    if (this.editMode){
      this.partnersService.editDriveRoutesData(form.value.time, this.driveRoutesListComponent.licensePlate, form.value.distance);
    }else {
      const indexOfPartnerId = location.pathname.split('/', 3).join('/').length;
      const partnerId = location.pathname.substring(10, indexOfPartnerId);
      const partnerName = this.partnersService.getPartnerName(+partnerId);
      this.partnersService.createDriveRoutes(form.value.time, this.driveRoutesListComponent.licensePlate, partnerName, form.value.distance);
    }
    this.driveRoutesListComponent.switchVisible();
    this.driveRoutesListComponent.driveRoutes = JSON.parse(localStorage.getItem('driveRoutes'));
    this.router.navigate([this.path], {relativeTo: this.route});
  }

  back(){
    this.driveRoutesListComponent.switchVisible();
  }

}
