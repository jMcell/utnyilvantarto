import {Component, Input, OnInit} from '@angular/core';
import {PartnersService} from '../partners.service';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css'],
  providers: [PartnersService]
})
export class CarListComponent implements OnInit {
  vehicle: {licensePlate: string, type: string, cc: number, fuelConsumption: number, partnerId: number}[] = [];
  id: number;
  carVisible = true;
  constructor(private partnersService: PartnersService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params.partnerId;
        }
      );
    this.vehicle = JSON.parse(localStorage.getItem('vehicle'));;
  }

  switchVisible(){
    this.carVisible = !this.carVisible;
  }
}
