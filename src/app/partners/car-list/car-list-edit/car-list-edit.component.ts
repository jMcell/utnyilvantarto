import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {CarListComponent} from '../car-list.component';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {PartnersService} from '../../partners.service';

@Component({
  selector: 'app-edit',
  templateUrl: './car-list-edit.component.html',
  styleUrls: ['./car-list-edit.component.css']
})
export class CarListEditComponent implements OnInit, AfterViewInit {
  @ViewChild('f', {static: false}) carListForm: NgForm;
  editMode: boolean;
  path: string;
  id: number;
  partnerID: number;
  vehicle: {
    licensePlate: string,
    type: string,
    cc: number,
    fuelConsumption: number,
    partnerId: number
  };
  constructor(private carListComponent: CarListComponent,
              private router: Router,
              private route: ActivatedRoute,
              private partnersService: PartnersService) { }

  ngOnInit(): void {
    if (this.carListComponent.carVisible === true){
      this.carListComponent.switchVisible();
    }
    this.route.params.subscribe(
      (params: Params ) => {
        this.id = params.id;
        this.partnerID = this.carListComponent.id;
        console.log(params);
      }
    );
    if (this.id == null){
      this.path = '../';
    }else{
      this.path = '../../';
    }
  }
  ngAfterViewInit(): void{
    if (this.id == null){
      this.editMode = false;
    }else {
      this.editMode = true;
      this.vehicle = this.partnersService.getVehicleData('' + this.id);
      setTimeout(() => {
        this.carListForm.setValue({
          licensePlate: this.vehicle.licensePlate,
          model: this.vehicle.type,
          cc: this.vehicle.cc,
          fuelConsumption : this.vehicle.fuelConsumption
        });
      });
    }
  }
  finish(form: NgForm){
    if (this.editMode){
      this.partnersService.editVehicleData(this.vehicle.licensePlate, form.value.licensePlate,
        form.value.model,
        form.value.cc,
        form.value.fuelConsumption,
        this.partnerID);
    }else {
      this.partnersService.createVehicleData(form.value.licensePlate,
                                             form.value.model,
                                             form.value.cc,
                                             form.value.fuelConsumption,
                                             this.partnerID);
    }
    this.carListComponent.switchVisible();
    this.carListComponent.vehicle = JSON.parse(localStorage.getItem('vehicle'));
    this.router.navigate([this.path], {relativeTo: this.route});
  }

  back(){
    this.carListComponent.switchVisible();
  }

}
