import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {PartnersComponent} from '../partners.component';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PartnersService} from '../partners.service';

@Component({
  selector: 'app-edit',
  templateUrl: './partners-edit.component.html',
  styleUrls: ['./partners-edit.component.css']
})
export class PartnersEditComponent implements OnInit, AfterViewInit {
  @ViewChild('f', {static: false}) partnerForm: NgForm;
  id: number;
  editMode: boolean;
  partner: {
    name: string,
    address: string
    id: number
  };
  constructor(private partnersComponent: PartnersComponent,
              private router: Router,
              private route: ActivatedRoute,
              private partnersService: PartnersService) { }
  ngOnInit(): void {
    if (this.partnersComponent.visible === true){
      this.partnersComponent.switchVisible();
    }
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params.id;
      }
    );

  }
  ngAfterViewInit(): void{
    if (this.id == null){
      this.editMode = false;
    }else {
      this.editMode = true;
      this.partner = this.partnersService.getPartnerData(this.id);
      setTimeout(() => {
        this.partnerForm.setValue({
          name: this.partner.name,
          address: this.partner.address
        });
      });
    }
  }
  finish(form: NgForm){
    if (this.editMode){
      this.partnersService.editPartnerData(form.value.name, form.value.address, this.id - 1);
    }
    else{
      this.partnersService.createPartnerData(form.value.name, form.value.address);
    }
    this.router.navigateByUrl('/partners');
    this.partnersComponent.partner = JSON.parse(localStorage.getItem('partner'));
    this.partnersComponent.switchVisible();
  }
  back(){
    this.partnersComponent.switchVisible();
  }
}
