export class PartnersService{
  visibe = true;

  getPartnerData(id: number){
    const partnerObj = JSON.parse(localStorage.getItem('partner'));
    for (const partner of partnerObj) {
      if (partner.id === +id){
        return partner;
      }
    }
  }
  createPartnerData(name: string, address: string){
    if (localStorage.getItem('partner') === null){
      const newPartner = [
        {
          name,
          address,
          id : 1
        }
      ];
      localStorage.setItem('partner', JSON.stringify(newPartner));
    } else {
      const retrievedObject = localStorage.getItem('partner');
      const partnerObj = JSON.parse(retrievedObject);
      const id = partnerObj.length + 1;
      partnerObj.push({name, address, id});
      localStorage.setItem('partner', JSON.stringify(partnerObj));
    }
  }
  editPartnerData(name: string, address: string, id){
    const tmpPartnerArray = JSON.parse(localStorage.getItem('partner'));
    const tmpDriveRoutesArray = JSON.parse(localStorage.getItem('driveRoutes'));
    let driveRouteIntex = 0;
    for (const driveroute of tmpDriveRoutesArray) {
      if (driveroute.partner === tmpPartnerArray[id].name) {
        tmpDriveRoutesArray[driveRouteIntex].partner = name;
      }
      driveRouteIntex += 1;
    }
    tmpPartnerArray[id].name = name;
    tmpPartnerArray[id].address = address;
    localStorage.setItem('driveRoutes', JSON.stringify(tmpDriveRoutesArray));
    localStorage.setItem('partner', JSON.stringify(tmpPartnerArray));
  }
  getVehicleData(id: string){
    const vehicleObj = JSON.parse(localStorage.getItem('vehicle'));
    for (const vehicle of vehicleObj) {
      if (vehicle.licensePlate === id){
        return vehicle;
      }
    }
  }
  createVehicleData(licensePlate: string, type: string, cc: number, fuelConsumption: number, partnerId: number){
    if (localStorage.getItem('vehicle') === null){
      const newVehicle = [
        {
          licensePlate,
          type,
          cc,
          fuelConsumption,
          partnerId
        }
      ];
      localStorage.setItem('vehicle', JSON.stringify(newVehicle));
    } else {
      const retrievedObject = localStorage.getItem('vehicle');
      const vehicleObj = JSON.parse(retrievedObject);
      vehicleObj.push({licensePlate, type, cc, fuelConsumption, partnerId});
      localStorage.setItem('vehicle', JSON.stringify(vehicleObj));
    }
  }
  editVehicleData(oldLicensePlate: string, licensePlate: string, type: string, cc: number, fuelConsumption, partnerId){
    const tmpVehicleArray = JSON.parse(localStorage.getItem('vehicle'));
    const tmpDriveRouteArray = JSON.parse(localStorage.getItem('driveRoutes'));
    let driveRouteIndex = 0;
    for (const driveRoute of tmpDriveRouteArray) {
      if (driveRoute.vehicle === oldLicensePlate) {
        tmpDriveRouteArray[driveRouteIndex].vehicle = licensePlate;
      }
      driveRouteIndex += 1;
    }
    let i = 0;
    while (i < tmpVehicleArray.length){
      if (tmpVehicleArray[i].licensePlate === oldLicensePlate){
        tmpVehicleArray[i].licensePlate = licensePlate;
        tmpVehicleArray[i].type = type;
        tmpVehicleArray[i].cc = cc;
        tmpVehicleArray[i].fuelConsumption = fuelConsumption;
      }
      i++;
    }
    localStorage.setItem('driveRoutes', JSON.stringify(tmpDriveRouteArray));
    localStorage.setItem('vehicle', JSON.stringify(tmpVehicleArray));
  }
  getDriveRoutes(id: string){
    const driveRoutesObj = JSON.parse(localStorage.getItem('driveRoutes'));
    for (const driveRoutes of driveRoutesObj) {
      if (driveRoutes.time === id){
        return driveRoutes;
      }
    }
  }
  createDriveRoutes(time: string, vehicle: string, partner: string, length: number){
    if (localStorage.getItem('driveRoutes') === null){
      const newVehicle = [
        {
          time,
          vehicle,
          partner,
          length
        }
      ];
      localStorage.setItem('driveRoutes', JSON.stringify(newVehicle));
    } else {
      const retrievedObject = localStorage.getItem('driveRoutes');
      const driveRoutesObj = JSON.parse(retrievedObject);
      driveRoutesObj.push({time, vehicle, partner, length});
      localStorage.setItem('driveRoutes', JSON.stringify(driveRoutesObj));
    }
  }
  editDriveRoutesData(time: string, vehicle: string, length: number){
    const tmpDriveRoutesArray = JSON.parse(localStorage.getItem('driveRoutes'));
    let i = 0;
    while (i < tmpDriveRoutesArray.length){
      console.log(tmpDriveRoutesArray[i].vehicle + ' ' + vehicle);
      if (tmpDriveRoutesArray[i].time === time){
        tmpDriveRoutesArray[i].time = time;
        tmpDriveRoutesArray[i].length = length;
       }
      i++;
    }
    localStorage.setItem('driveRoutes', JSON.stringify(tmpDriveRoutesArray));
  }
  getPartnerName(id: number){
    const partnerObj = JSON.parse(localStorage.getItem('partner'));
    for (const partner of partnerObj) {
      if (partner.id === +id){
        return partner.name;
      }
    }
  }

}
